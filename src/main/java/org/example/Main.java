package org.example;

import org.example.nymble.*;
import org.example.nymble.passenger.GoldPassenger;
import org.example.nymble.passenger.PremiumPassenger;
import org.example.nymble.passenger.StandardPassenger;

public class Main {
    public static void main(String[] args) {
        // Create travel package
        TravelPackage travelPackage = new TravelPackage("Amazing Trip", 100);

        // Create destinations
        Destination destination1 = new Destination("Destination 1");
        Destination destination2 = new Destination("Destination 2");

        // Create activities
        Activity activity1 = new Activity("Activity 1", "Description 1", 50.0, 20, destination1);
        Activity activity2 = new Activity("Activity 2", "Description 2", 80.0, 30, destination1);
        Activity activity3 = new Activity("Activity 3", "Description 3", 100.0, 15, destination2);
        Activity activity4 = new Activity("Activity 4", "Description 4", 120.0, 25, destination2);

        // Add activities to destinations
        destination1.addActivity(activity1);
        destination1.addActivity(activity2);
        destination2.addActivity(activity3);
        destination2.addActivity(activity4);

        // Add destinations to travel package
        travelPackage.addDestination(destination1);
        travelPackage.addDestination(destination2);

        // Create passengers
        StandardPassenger passenger1 = new StandardPassenger("John", 1, 200.0);
        GoldPassenger passenger2 = new GoldPassenger("Alice", 2, 300.0);
        PremiumPassenger passenger3 = new PremiumPassenger("Bob", 3);

        // Add passengers to travel package
        travelPackage.addPassenger(passenger1);
        travelPackage.addPassenger(passenger2);
        travelPackage.addPassenger(passenger3);

        // Print itinerary
        travelPackage.printItinerary();

        // Print passenger list
        travelPackage.printPassengerList();
    }
}
